var express = require('express'),
    photos = require(appRoot + '/app/controllers/photos.js');

var router = express.Router();


router.get('/', photos.index);

router.post('/add', photos.add);

router.get('/view/:id', photos.view);

router.get('/get/:id', photos.get);

router.post('/update/:id', photos.update);

router.delete('/delete/:id', photos.delete);

module.exports = router;
