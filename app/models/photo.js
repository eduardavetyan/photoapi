var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var photoSchema = new Schema({
    title: String,
    tags: Array,
    path: String,
    private: Boolean,
    user_id: String,
    coordinates: Array
});


var Photo = mongoose.model('Photo', photoSchema);

Photo.findUserPhotos = function( userId, foundPhotos ){

    Photo.find({user_id: userId}, function(err, photos) {

        foundPhotos( err, photos );
    });

};


module.exports = Photo;